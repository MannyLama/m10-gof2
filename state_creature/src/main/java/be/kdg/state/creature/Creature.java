package be.kdg.state.creature;

/**
 * Het gedrag van de methode greet is afhankelijk van de toestande van de boolean isFrog.
 * Pas op deze klasse het State pattern toe (maak gebruik van een interface).
 */
public class Creature {
    private CreatureState state;

    public Creature() {
        state = new Frog(this);
    }

    public String greet() {
        return state.makeSound();
    }

    public void kiss() {
        state = new Prince(this);
    }
}
