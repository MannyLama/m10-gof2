package be.kdg.state.creature;

public class Frog implements CreatureState{
    private final Creature creature;

    public Frog(Creature creature) {
        this.creature = creature;
    }

    @Override
    public String makeSound() {
        return "Ribbet!";
    }
}
