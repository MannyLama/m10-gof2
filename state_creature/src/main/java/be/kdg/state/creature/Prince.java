package be.kdg.state.creature;

public class Prince implements CreatureState {
    private final Creature creature;

    public Prince(Creature creature) {
        this.creature = creature;
    }

    @Override
    public String makeSound() {
        return "Derling!";
    }
}
