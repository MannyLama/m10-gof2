package be.kdg.state.creature;

public interface CreatureState {
    String makeSound();
}
